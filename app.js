"use strict";

global.CONFIG = require("./config.json");

const { resolve } = require("path");
const express = require("express");
const bodyParser = require("body-parser");
const UserRouter = require("./app/routers/UserRouter");
const RoomRouter = require("./app/routers/RoomRouter");

const StompitHandler = require('./app/controllers/StompitHandler');

const app = express();

//________________________
var http = require("http").createServer(app);
var io = require("socket.io")(http);
const socketEvents = require("./app/services/socketio/SocketioService")(io);

// * Setup stompit
const stompitHandler = new StompitHandler({to: global.CONFIG.topic});
stompitHandler.connect();


http.listen(CONFIG.port, () => {
  console.log(`Listening http://localhost:${CONFIG.port}`);
});
//________________________

app.use(bodyParser.json({}));
//app.use(bodyParser.text({}));
//app.use(bodyParser.raw({}));
//app.use(bodyParser.urlencoded({extended: true}));

app.use(function (req, res, next) {
  console.log(req.path);
  next();
});
app.use(express.static(CONFIG.publicDir));

app.use("/api", UserRouter);
app.use("/api", RoomRouter);

app.use(function (req, res) {
  console.warn(`${req.path} 404`);
  res.status(404);
  //res.sendFile(resolve(`${CONFIG.publicDir}/img/404.png`));
  res.redirect("/img/404.png");
});
