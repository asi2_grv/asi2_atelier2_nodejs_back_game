const UserDao = require('../dao/UserDao');

class UserService {
    constructor({}) {
        console.log(`new UserService`);
    }

    listUser() {
        return UserDao.getAllUsers();
    }

    getUser(userId) {
        return UserDao.getUser(userId);
    }

    addUser(user) {
        return UserDao.createUser(user);
    }

    resetPowerPoint(user){
        UserDao.resetPowerPoint(user);
    }
}

module.exports = new UserService({});