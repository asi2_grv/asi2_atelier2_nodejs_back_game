const Room = require("../../models/Room");
const User = require("../../models/User");
const RoomService = require("../RoomService");
const SocketioEvents = require("../../constants/SocketioEvents");
const UserService = require("../UserService");

function roomexists(room, roomse) {
  for (let i = 0; i < roomse.length; i++) {
    if (roomse[i].roomName === room.roomName) {
      return true;
    }
  }
  return false;
}

function check_new_turn(id, users){
  var new_turn_index = users.findIndex(user => user.id !== id );
  return users[new_turn_index].id;
}

exports = module.exports = function (io) {
  io.on(SocketioEvents.on.Connection, (socket) => {
    console.log("Socket IO connected");

    let currentUser = new User({ id :1, login :"test", surName:'tetet', lastName:'dgyf', pwd:'uyfsdgyu', email:'uyfsdgyu', cardList : [] });
    
    // Creates a room  and adds the current user
    // data contains the roomName and the user
    socket.on(SocketioEvents.on.CreationRoom, (data) => {
      const room = new Room(data.room);
      currentUser.setData(data.user);
      currentUser.resetCards();

      if (!roomexists(room, RoomService.listRoom())) {
        const roomnow = RoomService.addRoom(room);
        RoomService.addUser(roomnow, currentUser);
        socket.join(roomnow.id);
        socket.emit("sendRoomId", RoomService.getRoomId(roomnow.roomName));
        socket.emit("updaterooms", RoomService.listRoom());
      } else {
        socket.emit(SocketioEvents.emit.RoomDuplicate, "A room has already this name");
      }
    });

    // On opening new room sends the rooms to clients
    socket.on(SocketioEvents.on.NewPage, () => {
      socket.emit(SocketioEvents.emit.UpdateRooms, RoomService.listRoom());
    });

    // Adds user to the room
    // data contains the roomName and the user
    socket.on(SocketioEvents.on.AddUser, (data) => {
      let room = RoomService.getRoom(data.room.id);
      if(room.numberPlayer < 2){
        let user = new User(data.user);
        user.resetCards();
        let isJoining = RoomService.addUser(room, user);
        if(isJoining){
          socket.join(room.id);
          socket.emit("sendRoomId", RoomService.getRoomId(room.roomName));
          socket.emit("updaterooms", RoomService.listRoom());
        } else {
          socket.emit("errorInRoom", "Cette room est déjà pleine");
        }
      } else {
        socket.emit("errorInRoom", "Cette room est déjà pleine");
      }
    });

    // When users have selected their cards, adds them to the users cardList of the room
    // inform room users of the selected cards 
    socket.on(SocketioEvents.on.CardSelection, (data) => {
      let user = new User(data.user);
      let room = RoomService.getRoom(data.room[0].id);
      room.updatePlayerCards(user.id, data.cards);
      if(room.numberPlayer == 2 && room.users.filter(player => player.cardList.length !==0).length === 2){
        io.in(room.id).emit(SocketioEvents.emit.PlayerSelectCard, RoomService.getRoom(room.id));
        io.in(room.id).emit(SocketioEvents.emit.FirstPlayer, room.users[0].id)
      } else {
        socket.emit("waiting", "Waiting for a second player...");
      }
    });

    socket.on("attack", (body) => {
      let play_card = body.play_card;
      let attacked_card = body.attacked_card;
      let room = RoomService.getRoom(body.current_room.id);
      let values = RoomService.madeAttack(room, play_card, attacked_card);
      let endgame = values[0];
      let getEsquive = values[1];
      let getCritic = values[2];
      if(endgame){
        io.in(room.id).emit(SocketioEvents.emit.EndGame);
        io.sockets.adapter.rooms[room.id]?.forEach(function(s){
          s.leave(room.id);
        });
        RoomService.deleteRoom(room);
        socket.emit("updaterooms", RoomService.listRoom());
      } else {
        RoomService.updatePowerPoint(room, play_card.id);
        io.in(room.id).emit(SocketioEvents.emit.RenderRoom, RoomService.getRoom(room.id));
        if(getEsquive){
          socket.emit("attackMessage","L'ennemi a esquivé");
        } else {
          if(getCritic){
            socket.emit("attackMessage","BOOOOOOOOM Coup critique !");
          } else {
            socket.emit("attackMessage","");
          }
        }
      }
    });

    socket.on(SocketioEvents.on.ChangeUser, (body) => {
      let room = RoomService.getRoom(body.current_room.id);
      UserService.resetPowerPoint(room.users.find(user => user.id === body.id));
      socket.emit("attackMessage","");
      io.in(room?.id).emit(SocketioEvents.emit.RenderRoom, RoomService.getRoom(room.id));
      io.in(room?.id).emit(SocketioEvents.emit.UpdatePlayerTurn, check_new_turn(body.id, room.users));
    });

    socket.on(SocketioEvents.on.Disconnect, () => {
      console.log('disconected');
    });
  });
};
