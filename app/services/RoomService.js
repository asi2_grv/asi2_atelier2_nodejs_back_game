const RoomDao = require('../dao/RoomDao');
const StompitHandler = require('../controllers/StompitHandler');

class RoomService {
    constructor({}) {
        console.log(`new RoomService`);
        this.gameStompit = new StompitHandler({ to: global.CONFIG.topic_game });
        this.gameStompit.connect();
    }

    listRoom() {
        return RoomDao.getAllRooms();
    }

    getRoomId(room){
        return RoomDao.getRoomId(room);
    }

    getRoom(roomId) {
        return RoomDao.getRoom(roomId);
    }

    addRoom(room) {
        return RoomDao.createRoom(room);
    }

    deleteRoom(room) {
        RoomDao.deleteRoom(room);
    }

    addUser(room, user){
        return RoomDao.addUserInRoom(room, user);
    }

    madeAttack(room, play_card, attacked_card){
        let values = RoomDao.madeAttack(room, play_card, attacked_card);
        if(room.users[0].cardList.length === 0){ 
            this.gameStompit.sendMessage("gameResult", room.getResultMessage(1));
            return [true, values[0], values[1]];
        } else if(room.users[1].cardList.length === 0){ 
            this.gameStompit.sendMessage("gameResult", room.getResultMessage(0));
            return [true, values[0], values[1]];
        }
        return [false, values[0], values[1]];
    }

    updatePowerPoint(room, id){
        var user = room.users.find(user => user.id == id);
        user.setPowerPoint();
    }
}

module.exports = new RoomService({});