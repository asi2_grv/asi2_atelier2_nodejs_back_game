const User = require("./User");

class Room{
    constructor ({roomName, priceRoom}){
        this.roomName = roomName;
        this.priceRoom = priceRoom;
        this.numberPlayer = 0;
        this.users = [];
    }

    updatePlayerCards(userId, cards) {
        for(const user of this.users){
            if(user.id === userId){
                let u = new User(user);
                u.addCards(cards);
            }
        }
    }

    setUsers(users){
        if(users.id !== this.users.find(user => user.id === users.id)){
            this.users.push(users);
            this.numberPlayer++;
            return true;
        }
    }

    setUsersCardAfterAttack(id, played_card, attacked_card){
        var getEsquive = false;
        var getCritic = false;
        var attacked_user_index = this.users.findIndex(user => user.id !== id );
        var attacked_card_index = this.users[attacked_user_index].cardList.findIndex(card => card.id === attacked_card.id );
        var esquive = Math.random();
        var cc = Math.random();
        if(esquive > 0.9){
            getEsquive = true;
        } else {
            if(cc > 0.8){
                attacked_card.hp -= 2*played_card.attack;
                getCritic = true;
            } else {
                attacked_card.hp -= played_card.attack;
            }
        }
        if(attacked_card.hp <= 0){
            this.users[attacked_user_index].cardList.splice(attacked_card_index, 1);
        } else {
            this.users[attacked_user_index].cardList[attacked_card_index] = attacked_card;
        }
        return [getEsquive, getCritic];
    }

    getResultMessage(state){
        return { player1: this.users[0].id, player2: this.users[1].id, state: state, bet: this.priceRoom };
    }
}

module.exports = Room;