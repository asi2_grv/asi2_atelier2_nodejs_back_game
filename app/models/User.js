//TODO copy UserModel from Back Spring
class User {
  constructor({ id, login, surName, lastName, pwd, email, cardList = this.resetCards() }) {
    this.id = id;
    this.login = login;
    this.surName = surName;
    this.lastName = lastName;
    this.pwd = pwd;
    this.email = email;
    this.cardList = cardList;
    this.powerPoint = 3;
  }

  setData({ id, login, surName, lastName, pwd, email, cardList}) {
    this.id = id;
    this.login = login;
    this.surName = surName;
    this.lastName = lastName;
    this.pwd = pwd;
    this.email = email;
    this.cardList = cardList;
    this.powerPoint = 3;
  }

  resetCards() {
    this.cardList = [];
  }

  addCards(cards) {
    for (let i = 0; i < cards.length; i++) {
      this.addCard(cards[i]);
    }
  }

  addCard(card) {
    this.cardList.push(card);
  }

  setPowerPoint(){
    this.powerPoint -= 1;
  }

  resetPowerPoint(){
    this.powerPoint = 3;
  }
}

module.exports = User;
