class Transaction {
    constructor(type, object) {
        this.action = String(type);
        this.data = object;
    }
}
module.exports = Transaction;
