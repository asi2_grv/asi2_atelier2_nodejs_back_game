const {Router} = require('express');
const UserController = require('../controllers/UserController');

const BASE_PATH = '/users';

const UserRouter = Router();
module.exports = UserRouter;


UserRouter.route(BASE_PATH)
    .get(UserController.getUsers)
    .post(UserController.createUser);

UserRouter.route(`${BASE_PATH}/:userId`)
    .get(UserController.getUser);
