const {Router} = require('express');
const RoomController = require('../controllers/RoomController');

const BASE_PATH = '/rooms';

const RoomRouter = Router();
module.exports = RoomRouter;


RoomRouter.route(BASE_PATH)
    .get(RoomController.getRooms)
    .post(RoomController.createRoom);

RoomRouter.route(`${BASE_PATH}/:roomId`)
    .get(RoomController.getRoom);

RoomRouter.route(`${BASE_PATH}/getRoomId`)
    .get(RoomController.getRoomId);
