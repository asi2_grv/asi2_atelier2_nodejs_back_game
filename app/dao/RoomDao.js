const { listRoom } = require("../services/RoomService");

// Mock
const listRooms = [];

class RoomDao {
    constructor({}) {
        console.log(`new RoomDao`);
    }

    getAllRooms() {
        return listRooms;
    }

    getRoomId(room){
        let response = listRooms.filter(
            g => g.roomName === room
        );
        return response;
    }

    getRoom(roomId) {
        let result = listRooms.find(
            g => g.id === Number(roomId)
        );
        return(result);
    }

    createRoom(room) {
        room.id = listRooms.length + 1;
        //TODO change so that we can define the room name
        //room.roomName = 'new room ' + room.id;
        listRooms.push(room);
        return room;
    }

    addUserInRoom(room, user){
        return room.setUsers(user);
    }

    madeAttack(room, play_card, attacked_card){
        let id = play_card.id;
        let played_card = play_card.play_card;
        return room.setUsersCardAfterAttack(id, played_card, attacked_card);
    }

    deleteRoom(room){
        var room_index = listRooms.findIndex(r => r.id === room.id);
        if(room_index >= 0){
            listRooms.splice(room_index, 1);
        }
    }
}

module.exports = new RoomDao({});