const User = require('../models/User');

// Mock
const listUsers = [];
listUsers.push(
    new User({
        id: 1,
        lastName   : 'TOTO',
        surName: 'Toto',
        account   : 12,
        email  : 'toto@yopmail.de'
    }),
    new User({
        id: 2,
        lastName  : 'TATA',
        surName: 'Tata',
        account   : 12,
        email  : 'tata@yopmail.de'
    })
);

class UserDao {
    constructor({}) {
        console.log(`new UserDao`);
    }

    getAllUsers() {
        return listUsers;
    }

    getUser(userId) {
        return listUsers.filter(
            u => u.id === Number(userId)
        )
        .pop();
    }

    createUser(user) {
        user.id = listUsers.length+1;
        listUsers.push(user);
        return user;
    }

    resetPowerPoint(user){
        user.resetPowerPoint()
    }
}

module.exports = new UserDao({});