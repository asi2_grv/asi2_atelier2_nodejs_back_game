const SocketioEvents = {
  //TODO change naming (Front + Back)
  on:{
    NewPage : "new_page",
    CardSelection: "select_card",
    AddUser: "adduser",
    CreationRoom: "creationRoom",
    Connection: "connection",
    Disconnect: "disconnect",
    ChangeUser: "changeuser"
  },
  emit:{
    UpdateRooms: "updaterooms",
    RoomDuplicate: "roomduplicate",
    PlayerSelectCard: "player_select_card",
    RenderRoom: "renderroom",
    FirstPlayer: 'firstplayer',
    UpdatePlayerTurn: 'updateplayerturn',
    EndGame: 'endgame'
  }
};

module.exports = SocketioEvents;
