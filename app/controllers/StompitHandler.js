const stompit = require('stompit');

const Transaction = require('../models/Transaction');

class StompitHandler {
    constructor({ to, from }) {
        this.sendHeaders = {
            'destination': to
        };
        this.subHeaders = {
            'destination': from
        };
        this.connect = this.connect.bind(this);
        this.handleConnect = this.handleConnect.bind(this);
        this.handleDisconnect = this.handleDisconnect.bind(this);
        this.sendMessage = this.sendMessage.bind(this);
    }
    connect() {
        stompit.connect(global.CONFIG.connectOptions, this.handleConnect);
    }
    handleConnect(err, client) {
        if (err)
            return this.handleDisconnect();
        this.client = client;
        client.on('disconnected', this.handleDisconnect)
        client.on('error', this.handleDisconnect)
    }
    handleDisconnect() {
        this.client = null;
        setTimeout(this.connect, 5000);//Reconnect after 5s
    }
    sendMessage(type, data) {
        if (this.client !== null) {
            const frame = this.client.send(this.sendHeaders);
            const transaction = new Transaction(type, data);
            frame.write(JSON.stringify(transaction));
            frame.end();
        }
        else {
            // TODO: retry 
            // (setTimeout ? -> many timeouts)
            // * push queue list (class attribute) + settimeout (stored in attribute)
            // other ?
        }
    }
}
module.exports = StompitHandler;
