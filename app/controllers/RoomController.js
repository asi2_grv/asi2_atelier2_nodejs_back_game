const RoomService = require('../services/RoomService');
const Room = require('../models/Room');
const { response } = require('express');

class RoomController {
    constructor({}) {
        console.log(`new RoomController`);
    }

    getRooms(request, response) {
        response.json(RoomService.listRoom());
    }

    getRoomId(request, response){
        response.json(RoomService.getRoomId(room));
    }

    getRoom(request, response) {
        response.json(RoomService.getRoom(request.params.roomId));
    }

    createRoom(request, response) {
        let room = new Room(request.body);
        room = RoomService.addRoom(room);
        response.json(room);
    }

    updateRoom(request, response) {}

    deleteRoom(request, response) {}
}

module.exports = new RoomController({});